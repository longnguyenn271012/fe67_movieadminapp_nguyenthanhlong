import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Dashboard from "./views/Dashboard";
import InsertUser from "./views/InsertUser";
import SignIn from "./views/SignIn";
import { useEffect } from "react";
import { fetchMe } from "./store/actions/auth";
import { useDispatch } from "react-redux";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const taiKhoan = localStorage.getItem("taiKhoan");

    if (taiKhoan) {
      fetchMe(dispatch, taiKhoan);
    }
  }, [dispatch]);

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route path="/signin" component={SignIn} />
        <Route path="/insertuser" component={InsertUser} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
