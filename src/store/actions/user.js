import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const fetchUserList = (dispatch, soTrang) => {
  axios({
    method: "GET",
    url: `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDungPhanTrang?MaNhom=GP01&soTrang=${soTrang}&soPhanTuTrenTrang=20`,
  })
    .then((res) => {
      dispatch(createAction(actionType.SET_USER_LIST, res.data.items));
    })
    .catch((err) => {
      console.log(err);
    });
};

export const insertUser = (newUser, accessToken) => {
  axios({
    method: "POST",
    url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThemNguoiDung",
    data: newUser,
    headers: {
      Authorization: "Bearer " + accessToken,
    },
  })
    .then((res) => {
      alert("Thêm thành công user " + newUser.taiKhoan);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const updateUser = (updatedUser, accessToken) => {
  axios({
    method: "PUT",
    url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
    data: updatedUser,
    headers: {
      Authorization: "Bearer " + accessToken,
    },
  })
    .then((res) => {
      alert("Cập nhật thành công user " + updatedUser.taiKhoan);
    })
    .catch((err) => {
      // API trả lỗi: from origin 'http://localhost:3000' has been blocked by CORS policy: 
      // No 'Access-Control-Allow-Origin' header is present on the requested resource.
      // Không hiểu lỗi gì, trong khi test qua PostMan vẫn đúng
      console.log(err);
      alert("Cập nhật Không Thành Công!");
    });
};

export const deleteUser = (accessToken, taiKhoan) => {
  axios({
    method: "DELETE",
    url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung",
    params: {
      TaiKhoan: taiKhoan,
    },
    headers: {
      Authorization: "Bearer " + accessToken,
    },
  })
    .then((res) => {
      alert("Xóa Thành Công!");
    })
    .catch((err) => {
      //Err 500 (Internal Server Error) = "Người dùng này đã đặt vé xem phim không thể xóa!""
      console.log(err);
      alert("Xóa Không Thành Công!");
    });
};
