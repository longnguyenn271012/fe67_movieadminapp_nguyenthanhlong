export const actionType = {
  SET_ME: "SET_ME",
  SET_USER_LIST: "SET_USER_LIST",
  DELETE_USER: "DELETE_USER",
};
