import { actionType } from "./type";
import { createAction } from "./index";
import axios from "axios";

export const signIn = (dispatch, account) => {
  axios({
    method: "POST",
    url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
    data: account,
  })
    .then((res) => {
      if (res.data.maLoaiNguoiDung === "QuanTri") {
        dispatch(createAction(actionType.SET_ME, res.data));
        // API sử dụng taiKhoan để Lấy thông tin tài khoản
        // Nên lưu về localStorage taiKhoan
        localStorage.setItem("taiKhoan", res.data.taiKhoan);
        localStorage.setItem("accessToken", res.data.accessToken);
      }
    })
    .catch((err) => {
      console.log(err);
      alert("Đăng nhập không thành công !")
    });
};

export const fetchMe = async (dispatch, taiKhoan) => {
  try {
    const res = await axios({
      method: "POST",
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      data: { taiKhoan },
    });

    dispatch(createAction(actionType.SET_ME, res.data));
  } catch (err) {
    console.log(err);
  }
};
