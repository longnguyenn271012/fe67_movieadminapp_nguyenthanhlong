import React, { useEffect } from "react";
import {
  Button,
  TextField,
  Typography,
  Avatar,
  Container,
  withStyles,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import * as yup from "yup";
import Header from "../../components/Header";
import { useFormik } from "formik";
import { useDispatch, connect, useSelector } from "react-redux";
import { signIn } from "../../store/actions/auth";
import { styles } from "./style";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("Bạn không được bỏ trống ô này !"),
  matKhau: yup
    .string()
    .required("Bạn không được bỏ trống ô này !")
    .matches(/[A-Za-z0-9]{8,}$/g, "Mật khẩu tối thiểu 8 kí tự !"),
});

const SignIn = (props) => {
  const { paper, avatar, form, submit } = props.classes;

  const dispatch = useDispatch();

  const me = useSelector((state) => state.auth);

  const {
    touched,
    errors,
    isValid,
    values,
    handleChange,
    handleBlur,
    setTouched,
  } = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    setTouched({
      taiKhoan: true,
      matKhau: true,
    });

    if (!isValid) return;

    const account = {
      ...values,
    };

    signIn(dispatch, account);
  };

  useEffect(() => {
    if (me) {
      props.history.push("/");
    }
  });

  return (
    <div>
      <Header />

      <Container component="main" maxWidth="sm">
        <div className={paper}>
          <Avatar className={avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Thông Tin Đăng Nhập
          </Typography>
          <form className={form}>
            <TextField
              margin="dense"
              id="taiKhoan"
              label="Tài khoản"
              value={values.taiKhoan}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.taiKhoan && (
              <Typography variant="body2" color="secondary">
                {errors.taiKhoan}
              </Typography>
            )}
            <TextField
              margin="dense"
              id="matKhau"
              label="Mật khẩu"
              value={values.matKhau}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.matKhau && (
              <Typography variant="body2" color="secondary">
                {errors.matKhau}
              </Typography>
            )}
            <Button
              autoFocus
              className={submit}
              onClick={handleSubmit}
              variant="contained"
              color="primary"
              fullWidth
            >
              Đăng Nhập
            </Button>
          </form>
        </div>
      </Container>

      {/* <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        onSubmit={handleSubmit}
      >
        <DialogTitle id="form-dialog-title">Đăng Nhập</DialogTitle>
        <DialogContent>
          <TextField
            margin="dense"
            id="taiKhoan"
            label="Tài khoản"
            value={values.taiKhoan}
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.taiKhoan && (
            <Typography variant="body2" color="secondary">
              {errors.taiKhoan}
            </Typography>
          )}
          <TextField
            margin="dense"
            id="matKhau"
            label="Mật khẩu"
            value={values.matKhau}
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.matKhau && (
            <Typography variant="body2" color="secondary">
              {errors.matKhau}
            </Typography>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Hủy
          </Button>
          <Button autoFocus onClick={handleSubmit} color="primary">
            Đăng Nhập
          </Button>
        </DialogActions>
      </Dialog> */}
    </div>
  );
};

export default connect()(withStyles(styles)(SignIn));
