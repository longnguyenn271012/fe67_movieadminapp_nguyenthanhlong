import {
  Button,
  Container,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import { NotInterested, SkipNext, SkipPrevious } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Header from "../../components/Header";
import UserItem from "../../components/UserItem";
import { fetchUserList } from "../../store/actions/user";

const Dashboard = (props) => {
  const [pageNumber, setPageNumber] = useState(1);

  const dispatch = useDispatch();

  const me = useSelector((state) => state.auth);

  useEffect(() => {
    if (me) {
      fetchUserList(dispatch, pageNumber);
    } else {
      props.history.push("/signin");
    }
  }, [dispatch, pageNumber]);

  const userList = useSelector((state) => state.user.userList);

  return (
    <div>
      <Header />

      <Typography variant="h3" component="h3" align="center">
        Danh Sách Các Tài Khoản
      </Typography>

      {me ? (
        <NavLink to="/insertuser">
          <Button color="primary">Thêm Tài Khoản Mới</Button>
        </NavLink>
      ) : (
        <div />
      )}

      <TableContainer width="50%" component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>STT</TableCell>
              <TableCell>Tài Khoản</TableCell>
              <TableCell>Họ Tên</TableCell>
              <TableCell>Số Điện Thoại</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Loại Người Dùng</TableCell>
              <TableCell>Thao Tác</TableCell>
            </TableRow>
          </TableHead>
          {me ? (
            <TableBody>
              {userList.map((user) => (
                <UserItem user={user} stt={userList.indexOf(user) + 1} />
              ))}
            </TableBody>
          ) : (
            <TableBody>...</TableBody>
          )}
        </Table>
      </TableContainer>

      {/* Thanh chuyển trang */}
      {me ? (
        <Container maxWidth="xs">
          <Typography component="h6" variant="h6" align="center">
            Trang
          </Typography>
          <Grid container align="center">
            <Grid xs={4}>
              {pageNumber === 1 ? (
                <Button disabled>
                  <NotInterested />
                </Button>
              ) : (
                <Button onClick={() => setPageNumber(pageNumber - 1)}>
                  <SkipPrevious />
                </Button>
              )}
            </Grid>
            <Grid xs={4}>
              <Button>{pageNumber}</Button>
            </Grid>
            <Grid xs={4}>
              <Button onClick={() => setPageNumber(pageNumber + 1)}>
                <SkipNext />
              </Button>
            </Grid>
          </Grid>
        </Container>
      ) : (
        <div />
      )}
    </div>
  );
};

export default Dashboard;
