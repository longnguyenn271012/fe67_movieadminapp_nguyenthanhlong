import React, { useEffect, useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Typography,
} from "@material-ui/core";
import * as yup from "yup";
import { useFormik } from "formik";
import Header from "../../components/Header";
import { insertUser } from "../../store/actions/user";

const schema = yup.object().shape({
  taiKhoan: yup.string().required("Bạn không được bỏ trống ô này"),
  matKhau: yup.string().required("Bạn không được bỏ trống ô này"),
  hoTen: yup.string().required("Bạn không được bỏ trống ô này"),
  soDt: yup
    .string()
    .required("Bạn không được bỏ trống ô này")
    .matches(/^[0-9]+$/g, "Số điện thoại không hợp lệ !"),
  email: yup
    .string()
    .required("Bạn không được bỏ trống ô này")
    .email("Email không hợp lệ !"),
  maNhom: yup.string().required("Bạn không được bỏ trống ô này"),
  maLoaiNguoiDung: yup.string().required("Bạn không được bỏ trống ô này"),
});

const InsertUser = (props) => {
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);

    // Khi đóng form thêm user thì về luôn trang Dashboard
    props.history.push("/");
  };

  useEffect(() => {
    setOpen(true);
  }, []);

  const accessToken = localStorage.getItem("accessToken");

  const {
    isValid,
    setTouched,
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
  } = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      hoTen: "",
      email: "",
      soDt: "",
      maNhom: "",
      maLoaiNguoiDung: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleInsertUser = (event) => {
    event.preventDefault();

    setTouched({
      taiKhoan: true,
      matKhau: true,
      hoTen: true,
      email: true,
      soDt: true,
      maNhom: true,
      maLoaiNguoiDung: true,
    });

    if (!isValid) return;

    const newUser = {
      ...values,
    };

    insertUser(newUser, accessToken);

    handleClose();
  };

  return (
    <div>
      <Header />

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Thêm Thông Tin Người Dùng
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Hãy thêm các thông tin sau đây !
          </DialogContentText>
          <TextField
            margin="dense"
            id="taiKhoan"
            label="Tài Khoản"
            value={values.taiKhoan}
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.taiKhoan && (
            <Typography variant="body2" color="secondary">
              {errors.taiKhoan}
            </Typography>
          )}
          <TextField
            margin="dense"
            id="matKhau"
            label="Mật Khẩu"
            value={values.matKhau}
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.matKhau && (
            <Typography variant="body2" color="secondary">
              {errors.matKhau}
            </Typography>
          )}
          <TextField
            margin="dense"
            id="hoTen"
            label="Họ Tên"
            value={values.hoTen}
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.hoTen && (
            <Typography variant="body2" color="secondary">
              {errors.hoTen}
            </Typography>
          )}
          <TextField
            margin="dense"
            id="soDt"
            label="Số Điện Thoại"
            value={values.soDt}
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.taiKhoan && (
            <Typography variant="body2" color="secondary">
              {errors.taiKhoan}
            </Typography>
          )}
          <TextField
            margin="dense"
            id="email"
            label="Email"
            value={values.email}
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.email && (
            <Typography variant="body2" color="secondary">
              {errors.email}
            </Typography>
          )}
          <TextField
            margin="dense"
            id="maNhom"
            label="Mã Nhóm"
            value={values.maNhom}
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.maNhom && (
            <Typography variant="body2" color="secondary">
              {errors.maNhom}
            </Typography>
          )}
          <TextField
            margin="dense"
            id="maLoaiNguoiDung"
            label="Loại Người Dùng"
            value={values.maLoaiNguoiDung}
            type="text"
            onChange={handleChange}
            onBlur={handleBlur}
            fullWidth
          />
          {touched.maLoaiNguoiDung && (
            <Typography variant="body2" color="secondary">
              {errors.maLoaiNguoiDung}
            </Typography>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Hủy
          </Button>
          <Button autoFocus onClick={handleInsertUser} color="primary">
            Thêm
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default InsertUser;
