import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TableCell,
  TableRow,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import { deleteUser, updateUser } from "../../store/actions/user";
import * as yup from "yup";
import { useFormik } from "formik";

const schema = yup.object().shape({
  hoTen: yup.string().required("Bạn không được bỏ trống ô này"),
  soDt: yup
    .string()
    .required("Bạn không được bỏ trống ô này")
    .matches(/^[0-9]+$/g, "Số điện thoại không hợp lệ !"),
  email: yup
    .string()
    .required("Bạn không được bỏ trống ô này")
    .email("Email không hợp lệ !"),
  maLoaiNguoiDung: yup.string().required("Bạn không được bỏ trống ô này"),
});

const UserItem = (props) => {
  // Đóng mở form Update
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const { taiKhoan, matKhau, hoTen, soDt, email, maNhom, maLoaiNguoiDung } =
    props.user;

  const accessToken = localStorage.getItem("accessToken");

  const {
    touched,
    errors,
    isValid,
    values,
    handleChange,
    handleBlur,
    setTouched,
  } = useFormik({
    initialValues: {
      taiKhoan,
      matKhau,
      email,
      soDt,
      maNhom,
      maLoaiNguoiDung,
      hoTen,
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  const handleUpdateUser = (event) => {
    event.preventDefault();

    setTouched({
      taiKhoan: true,
      hoTen: true,
      email: true,
      soDt: true,
      maLoaiNguoiDung: true,
    });

    if (!isValid) return;

    const updatedUser = {
      ...values,
    };

    updateUser(updatedUser, accessToken);

    handleClose();
  };

  const handleDeleteUser = (taiKhoan) => {
    deleteUser(accessToken, taiKhoan);
  };

  return (
    <TableRow>
      <TableCell>{props.stt}</TableCell>
      <TableCell>{taiKhoan}</TableCell>
      <TableCell>{hoTen}</TableCell>
      <TableCell>{soDt}</TableCell>
      <TableCell>{email}</TableCell>
      <TableCell>{maLoaiNguoiDung}</TableCell>
      <TableCell>
        <Button color="primary" onClick={() => handleDeleteUser(taiKhoan)}>
          Xóa
        </Button>

        <Button color="secondary" onClick={handleClickOpen}>
          Sửa
        </Button>

        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
          onSubmit={handleUpdateUser}
        >
          <DialogTitle id="form-dialog-title">
            Sửa Thông Tin Người Dùng
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Hãy sửa thông tin mà bạn muốn !
            </DialogContentText>
            <TextField
              margin="dense"
              id="taiKhoan"
              label="Tài Khoản"
              value={taiKhoan}
              disabled
              fullWidth
            />
            <TextField
              margin="dense"
              id="hoTen"
              label="Họ Tên"
              value={values.hoTen}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.hoTen && (
              <Typography variant="body2" color="secondary">
                {errors.hoTen}
              </Typography>
            )}
            <TextField
              margin="dense"
              id="soDt"
              label="Số Điện Thoại"
              value={values.soDt}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.soDt && (
              <Typography variant="body2" color="secondary">
                {errors.soDt}
              </Typography>
            )}
            <TextField
              margin="dense"
              id="email"
              label="Email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.email && (
              <Typography variant="body2" color="secondary">
                {errors.email}
              </Typography>
            )}
            <TextField
              margin="dense"
              id="maLoaiNguoiDung"
              label="Loại Người Dùng"
              value={values.maLoaiNguoiDung}
              onChange={handleChange}
              onBlur={handleBlur}
              fullWidth
            />
            {touched.maLoaiNguoiDung && (
              <Typography variant="body2" color="secondary">
                {errors.maLoaiNguoiDung}
              </Typography>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Hủy
            </Button>
            <Button onClick={handleUpdateUser} color="primary">
              Sửa
            </Button>
          </DialogActions>
        </Dialog>
      </TableCell>
    </TableRow>
  );
};

export default UserItem;
