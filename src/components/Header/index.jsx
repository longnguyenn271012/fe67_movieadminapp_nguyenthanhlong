import React from "react";
import { NavLink } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  withStyles,
} from "@material-ui/core";
import MovieFilterIcon from "@material-ui/icons/MovieFilter";
import { Fragment } from "react";
import { styles } from "./style";
import { connect, useSelector } from "react-redux";

const Header = (props) => {
  const { title, navLink, activeNavLink } = props.classes;

  const me = useSelector((state) => state.auth);

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu">
          <MovieFilterIcon />
        </IconButton>
        <Typography variant="h6" className={title}>
          Movie Administration
        </Typography>

        <NavLink
          activeClassName={activeNavLink}
          className={navLink}
          exact
          to="/"
        >
          Home
        </NavLink>
        {me ? (
          <Fragment>
            <NavLink activeClassName={activeNavLink} className={navLink} to="/">
              Hello, {me.hoTen}
            </NavLink>
          </Fragment>
        ) : (
          <Fragment>
            <NavLink
              activeClassName={activeNavLink}
              className={navLink}
              to="/signin"
            >
              Sign in
            </NavLink>
          </Fragment>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default connect()(withStyles(styles)(Header));
